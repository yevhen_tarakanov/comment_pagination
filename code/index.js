const url = "https://jsonplaceholder.typicode.com/comments";
const req = new XMLHttpRequest();
req.open("GET", url);
req.send();
document.createElement("div").innerHTML = req.responseText;
req.addEventListener("readystatechange", () => {
    if (req.readyState === 4 && req.status >= 200 && req.status < 300) {
        showPeopleStarWar(JSON.parse(req.responseText));
    } else if (req.readyState === 4) {
        throw new Error("Помилка у запиті!");
    }
})

function showPeopleStarWar(data) {
    if (!Array.isArray(data)) {
        throw new Error("З сервера прийшов не масив!");
    }
    data.forEach((obj) => {
        createPeopleCard(obj)
    })
}

let cou = 1;
let divPage = document.querySelector('.page')

function createPeopleCard({postId, id, name, email, body, ...props}) {
    const div = document.createElement("div");
    div.setAttribute('data-num', `${cou}`)
    cou = cou + 1;
    div.className = 'comments';
    div.insertAdjacentHTML("beforeend", `
    <div class="text">Іd: ${id}</div>
    <div class="text">Ім'я: ${name}</div>
    <div class="text">email: <a href="mailto:${email}">${email}</a></div>

    <div class="text">body: ${body}</div> `);
    divPage.appendChild(div);
    return divPage;
}

let count = 500;
let cnt = 10;
let cnt_page = Math.ceil(count / cnt); //кол-во страниц

let paginator = document.querySelector(".paginator");
let page = "";
for (let i = 0; i < cnt_page; i++) {
    page += "<span data-page=" + i * cnt + "  id=\"page" + (i + 1) + "\">" + (i + 1) + "</span>";
}
paginator.innerHTML = page;

document.getElementById("swapi").addEventListener("click", () => {
    let divComments = divPage.children
    for (let i = 0; i < divComments.length; i++) {
        if (i < cnt) {
            divComments[i].style.display = "block";
        }
        let main_page = document.getElementById("page1");
        main_page.classList.add("paginator_active");

        paginator.addEventListener("click", function pagination(event) {
            let e = event || window.event;
            let target = e.target;
            let id = target.id;

            if (target.tagName.toLowerCase() !== "span") return;

            let data_page = +target.dataset.page;
            main_page.classList.remove("paginator_active");
            main_page = document.getElementById(id);
            main_page.classList.add("paginator_active");

            let j = 0;
            for (let i = 0; i < divComments.length; i++) {
                let data_num = divComments[i].dataset.num;
                if (data_num <= data_page || data_num >= data_page)
                    divComments[i].style.display = "none";

            }
            for (let i = data_page; i < divComments.length; i++) {
                if (j >= cnt) break;
                divComments[i].style.display = "block";
                j++;
            }
        })
    }
})